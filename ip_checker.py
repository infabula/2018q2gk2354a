def check_octet_validity(octet, min, max):
    value = int(octet)
    if not value in range(min, max):
        raise TypeError("Invalid octed")

    
def check_octet_list_validity(octet_string_list, line_number):
    for octet_index, string_octet in enumerate(octet_string_list):
        try:
            check_octet_validity(string_octet, 0, 255)
        except TypeError:
            raise TypeError("Invalid octet at position {} in line {}".format(octet_index,
                                                                             line_number))
 
def check_ip_validity(ip_string, line_number):
    cleaned_ip_string = ip_string.strip()
    octet_strings = cleaned_ip_string.split('.')
    if len(octet_strings) != 4:
        raise Exception("Not 4 octets on line {} ".format(line_number))
    else:
        check_octet_list_validity(octet_strings, line_number)

def proces_file(filename):
    with open(filename, 'r') as file:
        for line_index, line in enumerate(file):
            line_number = line_index + 1
            try:
                check_ip_validity(line, line_number)
            except Exception as e:
                print(e)

proces_file("ip_address.txt")
        
