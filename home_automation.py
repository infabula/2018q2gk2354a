home = {
    'floors': [ { 'name': 'ground floor',
                  'index': 0,
                  'accessible': True,
                  'rooms': [ { 'name': 'kitchen',
                               'lights': [ { 'ip_address': '192.168.4.33',
                                             'name': 'afzuigkap'
                                          }
                                       ],
                               'devices': [ { 'name': 'microwave',
                                              'ip_address': '192.168.4.34',
                                              'status': 'off'
                                            },
                                            {
                                              'name': 'blender',
                                              'ip_address': '192.168.4.36',
                                              'status': 'on'
                                            }
                                   ]
                              }   

                         ]
                },
                { 'name': 'first floor',
                  'index': 1,
                  'accessible': True

                },
                { 'name': 'attic',
                  'index': 2,
                  'accessible': False
                }
    ]
}


def get_nb_of_floors(home):
    '''Returns the total number of floors of the home.'''
    floor_list = home['floors']
    nb_of_floors = len(floor_list)
    return nb_of_floors

def print_floor_names(home):
    ''' Prints the names of all the floors in the home.'''
    floor_list = home['floors']
    for floor in floor_list:
        print(floor['name'])

def get_floor(home, name):
    floor_list = home['floors']
    for floor in floor_list:
        if name == floor['name']:
            return floor
    raise Exception("Floor not found")

def get_room(floor, name):
    for room in floor['rooms']:
        if room['name'] == name:
            return room
    raise Exception("Room not found.")
    

def add_device(home, floor_name, room_name):
    '''Add a device to a room on the specified floor.'''
    # add to list : list.append()
    

if __name__ == "__main__":
    nb_of_floors = get_nb_of_floors(home)
    print("The home has {} floors.".format(nb_of_floors))
    print("The floors are:\n")
    print_floor_names(home)
    try:
        the_floor_we_want = get_floor(home, name='ground floor')
        print("Found floor {}".format(the_floor_we_want['name']))
        the_room_we_want = get_room(the_floor_we_want, 'kitchen')
        print("Found room {}".format(the_room_we_want['name']))
        
        koffee_machine = { 'name': 'koffee machine',
                           'ip_address': '192.168.4.56',
                           'status': 'on'
                         }
        the_room_we_want['devices'].append(koffee_machine)
        print("Devices:")
        for device in the_room_we_want['devices']:
            print(device['name'])

        
    except Exception as e:
        print(e)
    
