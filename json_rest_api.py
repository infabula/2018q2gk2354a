import json
import requests
subject = input("Which subject would you like to view? ")
# clean input
cleaned_subject = subject.strip()

url = 'https://openlibrary.org/subjects/{}.json'.format(cleaned_subject)

api_session = requests.session() # create a session object
response = api_session.get(url) # do a GET request

if response.status_code == 200: # test for success
    response = json.loads(response.text) # convert to python dict
    for work in response['works']:
        print(work['title'])    
else:
    print("Could not fetch data from %s", url)
