'''Domotica module.'''
import devices


def get_current_light_intensity():
    '''Measure the light intensity from a device.
    
    Returns values from 0.0 up to 1.0
    '''
    return 0.22  # fixture


def to_dark(threshold=0.3):
    '''Determine the light intensity.

       Returns True if the light intensity is smaller that the threshold, else false.
    '''

    if threshold < 0.0:
        raise ValueError("Threshold value is out of bounds.")

    
    current_light_intensity = get_current_light_intensity()  # a value between 0.0 and 1.0
    if current_light_intensity < threshold:
        return True
    
    return False 

def switch_on_light():
    '''Switch on the light.'''
    print("Switching on the light")
    
def main():
    try: 
        if to_dark(threshold=0.4):
            switch_on_light()  # which light?
    except Exception as e:
        print("Exception!: {}".format(e.text))
        
main()
