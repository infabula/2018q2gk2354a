import sys

if len(sys.argv) != 2:
    print("Usage:.....")
    sys.exit(1)

print("Opening file {}".format(sys.argv[1]))
